package org.cryptable.timestamper.controller;

import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampResponseGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.cardplus.timestamper.security.Authorization;
import se.cardplus.timestamper.security.AuthorizationException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

@RestController
public class TimestamperController {

    @Value("${tokenkey}")
    String tokenKey;

    @Value("${privatekey}")
    String privateKeyFile;

    @RequestMapping("/tsa")
    public ResponseEntity timstampHandler(@RequestHeader(value="Autorization") String authorization,
                                          @RequestBody byte[] request) throws
            NoSuchPaddingException,
            NoSuchAlgorithmException,
            AuthorizationException,
            BadPaddingException,
            InvalidKeyException,
            IllegalBlockSizeException,
            IOException {
        Authorization authString = new Authorization("79eb1b22f657816079W:A:Mozart:20" +
                "5853950819d07ad408dda6c30feece735f059e6eb9a4a9");
        String customerToken = authString.getAuthorizedToken(authorization);

        TimeStampRequest timeStampRequest = new TimeStampRequest(request);
        byte[] messageDigest = timeStampRequest.getMessageImprintDigest();
        TimeStampResponseGenerator tsaResponseGenerator = new TimeStampResponseGenerator();
        // TODO:  verify token from the list

        byte[] response = null;
        DigestCalculator dgCalc = new JcaDigestCalculatorProviderBuilder().build();
        SignerInfoGenerator siGen = new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder()).build(signer, getCertificate());
        ASN1ObjectIdentifier policy = new ASN1ObjectIdentifier("1.2.3.4.5.6"); // Replace by your timestamping policy OID
        TimeStampTokenGenerator tstGen = new TimeStampTokenGenerator(siGen, dgCalc, policy);
        /* Set the parameters e.g. set the accuracy or include the signing certificate */
        TimeStampToken tst = tstGen.generate(tsReq, generateSerialNumber(), new Date());
        byte[] encoding = tst.getEncoded();


        return ResponseEntity.ok(response);
    }

    private PrivateKey getPrivateKey() {

    }
}
