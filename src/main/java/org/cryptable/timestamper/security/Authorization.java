package org.cryptable.timestamper.security;

import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.encoders.HexEncoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class Authorization {

    Cipher cipher;
    Key key;

    public Authorization() {
        cipher = null;
        key = null;
    }

    public Authorization(String hexKey) throws NoSuchPaddingException, NoSuchAlgorithmException, AuthorizationException {
        byte[] aesKey = Hex.decode(hexKey);

        if ((aesKey.length != 16) && (aesKey.length != 32)) {
            throw new AuthorizationException("Invalid AES Key length (16 or 32)");
        }
        
        cipher = Cipher.getInstance("AES/GCM/NoPadding");
        key = new SecretKeySpec(aesKey, "AES");
    }

    public String getAuthorizedToken(String bearerToken) throws InvalidKeyException, AuthorizationException, BadPaddingException, IllegalBlockSizeException {
        String[] decodeToken = bearerToken.split("\\s+");

        if (decodeToken[0].equalsIgnoreCase("bearer")) {
            throw new AuthorizationException("No Bearer token found");
        }

        byte[] encryptedToken = Base64.decode(decodeToken[1].getBytes());

        cipher.init(Cipher.DECRYPT_MODE, key);
        String token = new String(cipher.doFinal(encryptedToken));

        return token;
    }
}
