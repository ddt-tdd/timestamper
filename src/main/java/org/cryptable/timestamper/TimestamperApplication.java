package org.cryptable.timestamper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimestamperApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimestamperApplication.class, args);
    }
}
