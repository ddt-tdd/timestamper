package org.cryptable.timestamper.business;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cms.SignerInfoGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampTokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Component
public class Timestamper {


    //TODO: Move to policy
    @Value("digestAglorithm")
    String digestAlgorithm;

    @Value("signatureAlgorithm")
    String signatureAlgorithm;

    PrivateKey signerPrivateKey;

    X509Certificate signerCertificate;

    public Timestamper(@Value("${privatekeyURN}") String privateKeyURN) {
        signerPrivateKey = null;
        signerCertificate = null;
    }

    public void addPolicy(String policyOID) {

    }

    byte[] process(byte[] request) throws OperatorCreationException, IOException, TSPException, CertificateEncodingException {
        TimeStampRequest tsq = new TimeStampRequest(request);
        DigestCalculator dgCalc = (new JcaDigestCalculatorProviderBuilder()).build()
                .get((new DefaultDigestAlgorithmIdentifierFinder().find(digestAlgorithm)));
        ContentSigner signer = new JcaContentSignerBuilder(signatureAlgorithm).build(signerPrivateKey);
        SignerInfoGenerator siGen = new JcaSignerInfoGeneratorBuilder((new JcaDigestCalculatorProviderBuilder()).build()).build(signer, signerCertificate);

        ASN1ObjectIdentifier policy = new ASN1ObjectIdentifier("1.2.3.4.5.6"); // Replace by your timestamping policy OID
        TimeStampTokenGenerator tstGen = new TimeStampTokenGenerator(siGen, dgCalc, policy);

        // TODO: needed top cenvert compouter time to UTC
        /*
         Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneOffset.UTC);
        Date out = Date.from(ldt.atZone(ZoneOffset.UTC).toInstant());
          */

        return tstGen.generate(tsq, BigInteger.ONE, new Date()).getEncoded();
    }
}
